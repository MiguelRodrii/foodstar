package foodstarEJB.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_de_persona database table.
 * 
 */
@Entity
@Table(name="tipo_de_persona")
@NamedQuery(name="TipoDePersona.findAll", query="SELECT t FROM TipoDePersona t")
public class TipoDePersona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_tipo_p")
	private Boolean idTipoP;

	@Column(name="descripcion_t_d_p")
	private String descripcionTDP;

	//bi-directional many-to-one association to Persona
	@OneToMany(mappedBy="tipoDePersona")
	private List<Persona> personas;

	public TipoDePersona() {
	}

	public Boolean getIdTipoP() {
		return this.idTipoP;
	}

	public void setIdTipoP(Boolean idTipoP) {
		this.idTipoP = idTipoP;
	}

	public String getDescripcionTDP() {
		return this.descripcionTDP;
	}

	public void setDescripcionTDP(String descripcionTDP) {
		this.descripcionTDP = descripcionTDP;
	}

	public List<Persona> getPersonas() {
		return this.personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public Persona addPersona(Persona persona) {
		getPersonas().add(persona);
		persona.setTipoDePersona(this);

		return persona;
	}

	public Persona removePersona(Persona persona) {
		getPersonas().remove(persona);
		persona.setTipoDePersona(null);

		return persona;
	}

}