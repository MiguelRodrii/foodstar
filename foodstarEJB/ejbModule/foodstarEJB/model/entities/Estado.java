package foodstarEJB.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the estado database table.
 * 
 */
@Entity
@NamedQuery(name="Estado.findAll", query="SELECT e FROM Estado e")
public class Estado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_estado")
	private Boolean idEstado;

	@Column(name="descripcion_est")
	private String descripcionEst;

	//bi-directional many-to-one association to Persona
	@OneToMany(mappedBy="estado")
	private List<Persona> personas;

	//bi-directional many-to-one association to Restaurante
	@OneToMany(mappedBy="estado")
	private List<Restaurante> restaurantes;

	public Estado() {
	}

	public Boolean getIdEstado() {
		return this.idEstado;
	}

	public void setIdEstado(Boolean idEstado) {
		this.idEstado = idEstado;
	}

	public String getDescripcionEst() {
		return this.descripcionEst;
	}

	public void setDescripcionEst(String descripcionEst) {
		this.descripcionEst = descripcionEst;
	}

	public List<Persona> getPersonas() {
		return this.personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public Persona addPersona(Persona persona) {
		getPersonas().add(persona);
		persona.setEstado(this);

		return persona;
	}

	public Persona removePersona(Persona persona) {
		getPersonas().remove(persona);
		persona.setEstado(null);

		return persona;
	}

	public List<Restaurante> getRestaurantes() {
		return this.restaurantes;
	}

	public void setRestaurantes(List<Restaurante> restaurantes) {
		this.restaurantes = restaurantes;
	}

	public Restaurante addRestaurante(Restaurante restaurante) {
		getRestaurantes().add(restaurante);
		restaurante.setEstado(this);

		return restaurante;
	}

	public Restaurante removeRestaurante(Restaurante restaurante) {
		getRestaurantes().remove(restaurante);
		restaurante.setEstado(null);

		return restaurante;
	}

}