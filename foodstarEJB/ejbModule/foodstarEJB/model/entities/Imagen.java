package foodstarEJB.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the imagen database table.
 * 
 */
@Entity
@NamedQuery(name="Imagen.findAll", query="SELECT i FROM Imagen i")
public class Imagen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_i")
	private Integer idI;

	@Column(name="descripcion_i")
	private String descripcionI;

	private String ruta;

	//bi-directional many-to-one association to Restaurante
	@ManyToOne
	@JoinColumn(name="id_establecimiento")
	private Restaurante restaurante;

	public Imagen() {
	}

	public Integer getIdI() {
		return this.idI;
	}

	public void setIdI(Integer idI) {
		this.idI = idI;
	}

	public String getDescripcionI() {
		return this.descripcionI;
	}

	public void setDescripcionI(String descripcionI) {
		this.descripcionI = descripcionI;
	}

	public String getRuta() {
		return this.ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public Restaurante getRestaurante() {
		return this.restaurante;
	}

	public void setRestaurante(Restaurante restaurante) {
		this.restaurante = restaurante;
	}

}