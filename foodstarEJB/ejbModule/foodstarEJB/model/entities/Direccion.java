package foodstarEJB.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the direccion database table.
 * 
 */
@Entity
@NamedQuery(name="Direccion.findAll", query="SELECT d FROM Direccion d")
public class Direccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="calle_principal")
	private String callePrincipal;

	@Column(name="calle_secundaria")
	private String calleSecundaria;

	//bi-directional many-to-one association to Restaurante
	@OneToMany(mappedBy="direccion")
	private List<Restaurante> restaurantes;

	public Direccion() {
	}

	public String getCallePrincipal() {
		return this.callePrincipal;
	}

	public void setCallePrincipal(String callePrincipal) {
		this.callePrincipal = callePrincipal;
	}

	public String getCalleSecundaria() {
		return this.calleSecundaria;
	}

	public void setCalleSecundaria(String calleSecundaria) {
		this.calleSecundaria = calleSecundaria;
	}

	public List<Restaurante> getRestaurantes() {
		return this.restaurantes;
	}

	public void setRestaurantes(List<Restaurante> restaurantes) {
		this.restaurantes = restaurantes;
	}

	public Restaurante addRestaurante(Restaurante restaurante) {
		getRestaurantes().add(restaurante);
		restaurante.setDireccion(this);

		return restaurante;
	}

	public Restaurante removeRestaurante(Restaurante restaurante) {
		getRestaurantes().remove(restaurante);
		restaurante.setDireccion(null);

		return restaurante;
	}

}