package foodstarEJB.model.managers;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import foodstarEJB.model.entities.Direccion;
import foodstarEJB.model.entities.Estado;
import foodstarEJB.model.entities.Imagen;
import foodstarEJB.model.entities.Persona;
import foodstarEJB.model.entities.Restaurante;

/**
 * Session Bean implementation class ManagerRestaurante
 */
@Stateless
@LocalBean
public class ManagerRestaurante {
	@PersistenceContext
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerRestaurante() {
		// TODO Auto-generated constructor stub
	}

	public List<Restaurante> findAllRestaurantes() {
		return em.createNamedQuery("Restaurante.findAll", Restaurante.class).getResultList();
	}

	public void registrarRestaurante(String username, String callePrincipal, String nombre, String descripcion,
			String imagenInicial, String descripcionImagen) throws Exception {
		System.out.println("Inicia busqueda de persona para el usuario: " + username);
		Persona p;
		p = em.find(Persona.class, username);
		System.out.println("Persona encontrada correctamente");

		if (p == null) {
			throw new Exception("La persona a la que esta intentando asociar este restaurante no existe");
		}
		System.out.println("Inicia busqueda de direccion");
		Direccion d;
		d = em.find(Direccion.class, callePrincipal);
		System.out.println("Direccion encontrada de forma exitos");
		if (d == null) {
			throw new Exception("La direcci�n especificada no exite, por favor reg�strela antes");
		}
		if (nombre.isEmpty()) {
			throw new Exception("Debe ingresar un nombre");
		}
		if (descripcion.isEmpty()) {
			throw new Exception("Debe ingresar una descripcion");
		}

		System.out.println("Creacion de restaurante - inicio asignacion de campos");
		Restaurante r;
		r = new Restaurante();

		r.setPersona(p);
		r.setDireccion(d);
		r.setNombre(nombre);
		r.setDescripcion(descripcion);
		r.setEstado(em.find(Estado.class, true));
		r.setCalificacion(new BigDecimal(0));
		r.setReportes(0);
		System.out.println("Asignacion de campos completada");

		System.out.println("Inicia enviado de datos a la bdd");
		try {
			em.persist(r);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception("Error en el registro de restaurante");
		}
		System.out.println("Enviado de datos del restaurante completado");

		if (!imagenInicial.isEmpty()) {
			if (!descripcionImagen.isEmpty()) {
				System.out.println("Creacion de imagen iniciada");
				Imagen i;
				i = new Imagen();
				System.out.println("seteo de datos");
				i.setRuta(imagenInicial);
				i.setDescripcionI(descripcionImagen);
				i.setRestaurante(r);

				try {
					em.persist(i);
				} catch (Exception e) {
					// TODO: handle exception
					throw new Exception("Error en el ingreso de datos (secci�n) imagen.");
				}
			} else {
				throw new Exception("Debe ingresar una descripci�n tambi�n para su imagen inicial");
			}
		}
	}

	public List<Restaurante> findAllRestaurantesByUsername(String user) {
		Query q;
		q = em.createQuery("select r from Restaurante r where r.persona.username=:user", Restaurante.class);
		q.setParameter("user", user);
		return q.getResultList();
	}
	
	public List<Restaurante> find
	
	
}
