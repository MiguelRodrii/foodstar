package foodstarEJB.model.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import foodstarEJB.model.entities.Estado;
import foodstarEJB.model.entities.Persona;
import foodstarEJB.model.entities.TipoDePersona;
import jdk.nashorn.internal.runtime.ECMAException;

/**
 * Session Bean implementation class ManagerPersona
 */
@Stateless
@LocalBean
public class ManagerPersona {

	@PersistenceContext
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerPersona() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Metodo para iniciar sesi�n en el sistema
	 * 
	 * @param username    Nombre de usuario provisto por la persona que usa el
	 *                    sistema
	 * @param contrasenia Contrase�a del usuario provista por la persona que usa
	 *                    el sistema
	 * @return True en caso de ingresar como administrador o False en caso de entrar
	 *         como usuario
	 * @throws Exception En caso de error en el login
	 */
	public boolean iniciarSesion(String username, String contrasenia) throws Exception {
		if (username.isEmpty()) {
			throw new Exception("Debe ingresar un nombre de usuario");
		}
		if (contrasenia.isEmpty()) {
			throw new Exception("Debe ingresar una contrase�a");
		}

		Persona p;
		p = em.find(Persona.class, username);

		if (p != null) {
			if (p.getContrasenia().equals(contrasenia)) {
				if (p.getEstado().getIdEstado()) {
					return p.getTipoDePersona().getIdTipoP();
				} else {
					throw new Exception("Su cuenta ha sido baneada.");
				}
			} else {
				throw new Exception("Contrase�a incorrecta");
			}
		} else {
			throw new Exception("Usuario no encontrado");
		}
	}

	public List<Persona> findAllPersona() {
		return em.createNamedQuery("Persona.findAll", Persona.class).getResultList();
	}

	public void registrarUsuario(String username, String correoElectronico, String contrasenia, String nombre,
			String apellido) throws Exception {
		registrarPersona(username, false, correoElectronico, contrasenia, nombre, apellido);
	}

	public void registrarAdministrador(String username, String correoElectronico, String contrasenia, String nombre,
			String apellido) throws Exception {
		registrarPersona(username, true, correoElectronico, contrasenia, nombre, apellido);
	}

	public void registrarPersona(String username, boolean id_tipo_p, String correoElectronico, String contrasenia,
			String nombre, String apellido) throws Exception {
		Persona p;
		p = new Persona();

		if (username.isEmpty()) {
			throw new Exception("Debe ingresar un nombre de usuario");
		}
		if (correoElectronico.isEmpty()) {
			throw new Exception("Debe ingresar un correo electronico");
		}
		if (contrasenia.isEmpty()) {
			throw new Exception("Debe ingresar una contrasenia");
		}
		if (nombre.isEmpty()) {
			throw new Exception("Debe ingresar un nombre");
		}
		if (apellido.isEmpty()) {
			throw new Exception("Debe ingresar un apellido");
		}

		p.setUsername(username);
		p.setTipoDePersona(em.find(TipoDePersona.class, id_tipo_p));
		p.setEstado(em.find(Estado.class, true));
		p.setCorreoElectronico(correoElectronico);
		p.setContrasenia(contrasenia);
		p.setNombre(nombre);
		p.setApellido(apellido);

		try {
			em.persist(p);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception("Error en el registro");
		}
	}

	public void banearUsuario(String username) throws Exception {
		Persona p;
		p = em.find(Persona.class, username);
		if (p == null) {
			throw new Exception("Usuario no encontrado");
		}
		if (p.getTipoDePersona().getIdTipoP()) {
			throw new Exception("No es posible banear administradores");
		}

		p.setEstado(em.find(Estado.class, false));
		try {
			em.merge(p);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception("Se ha producido un error en el baneo");
		}
	}
	
	public void desBanearUsuario(String username) throws Exception {
		Persona p;
		p = em.find(Persona.class, username);
		if (p == null) {
			throw new Exception("Usuario no encontrado");
		}

		p.setEstado(em.find(Estado.class, true));
		try {
			em.merge(p);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception("Se ha producido un error en el desbaneo");
		}
	}

	public void modificarPersona(Persona p, boolean id_tipo_p, boolean estado, String correoElectronico,
			String contrasenia, String nombre, String apellido) throws Exception {
		if (p == null) {
			throw new Exception("Usuario no encontrado");
		}
		if (correoElectronico.isEmpty()) {
			throw new Exception("Debe ingresar un correo electronico");
		}
		if (contrasenia.isEmpty()) {
			throw new Exception("Debe ingresar una contrasenia");
		}
		if (nombre.isEmpty()) {
			throw new Exception("Debe ingresar un nombre");
		}
		if (apellido.isEmpty()) {
			throw new Exception("Debe ingresar un apellido");
		}

		p.setTipoDePersona(em.find(TipoDePersona.class, id_tipo_p));
		p.setEstado(em.find(Estado.class, estado));
		p.setCorreoElectronico(correoElectronico);
		p.setContrasenia(contrasenia);
		p.setNombre(nombre);
		p.setApellido(apellido);

		try {
			em.merge(p);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception("Error en la modificaci�n");
		}
	}

	public void modificarUsuario(String username, boolean estado,
			String correoElectronico, String contrasenia, String nombre, String apellido) throws Exception {
		Persona p;
		p = em.find(Persona.class, username);
		if (p.getTipoDePersona().getIdTipoP()) {
			throw new Exception("No es posible modificar administradores");
		}
		modificarPersona(p, false, estado, correoElectronico, contrasenia, nombre, apellido);
	}
	
	public void eliminarPersona(Persona p) throws Exception {
		if (p == null) {
			throw new Exception("No existe ese registro");
		}
		try {
			em.remove(p);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception("Error en la eliminaci�n");
		}
	}
	
	public void eliminarUsuario(String username) throws Exception {
		Persona p;
		p = em.find(Persona.class, username);
		if(p.getTipoDePersona().getIdTipoP()) {
			throw new Exception("No es posible eliminar administradores");
		}
		eliminarPersona(p);
	}
	
	
}
