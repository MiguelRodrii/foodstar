package foodstarWeb.controller;

import java.io.Serializable;
import java.security.Permissions;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import foodstarEJB.model.managers.ManagerPersona;
import foodstarWeb.view.util.Notification;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import foodstarEJB.model.entities.Persona;

@Named
@SessionScoped
public class BeanAdministrarUsuarios implements Serializable {

	@EJB
	private ManagerPersona m;
	private Persona personaModificada;
	private List<Persona> listapersona;

	public BeanAdministrarUsuarios() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {
		listapersona = m.findAllPersona();
	}

	public String actionCerrarSesion() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/index?faces-redirect=true";
	}

	public void actionListenerRefrescar() {
		listapersona = m.findAllPersona();
	}

	public String actionRegresar() {
		return "indexAdministrador?faces-redirect=true";
	}

	public void actionListenerImprimirReporteDeUsuariosConRestaurantes() {
		Map<String, Object> parametros;
		parametros = new HashMap<>();
		FacesContext context;
		context = FacesContext.getCurrentInstance();
		ServletContext servletContext;
		servletContext = (ServletContext) context.getExternalContext().getContext();
		String path;
		path = servletContext.getRealPath("administrador/usuariosConRestaurante.jasper");
		System.out.println(path);
		HttpServletResponse response;
		response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=usuarios.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/foodstar", "postgres", "admin");
			JasperPrint impresion = JasperFillManager.fillReport(path, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			Notification.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerImprimirReporteDeUsuariosSinRestaurantes() {
		Map<String, Object> parametros;
		parametros = new HashMap<>();
		FacesContext context;
		context = FacesContext.getCurrentInstance();
		ServletContext servletContext;
		servletContext = (ServletContext) context.getExternalContext().getContext();
		String path;
		path = servletContext.getRealPath("administrador/usuariosSinRestaurante.jasper");
		System.out.println(path);
		HttpServletResponse response;
		response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=usuarios.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/foodstar", "postgres", "admin");
			JasperPrint impresion = JasperFillManager.fillReport(path, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			Notification.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerImprimirReporteDeTodosLosUsuarios() {
		Map<String, Object> parametros;
		parametros = new HashMap<>();
		FacesContext context;
		context = FacesContext.getCurrentInstance();
		ServletContext servletContext;
		servletContext = (ServletContext) context.getExternalContext().getContext();
		String path;
		path = servletContext.getRealPath("administrador/todosLosUsuarios.jasper");
		System.out.println(path);
		HttpServletResponse response;
		response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=usuarios.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/foodstar", "postgres", "admin");
			JasperPrint impresion = JasperFillManager.fillReport(path, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			Notification.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerBanearUsuario(String username) {
		try {
			m.banearUsuario(username);
			Notification.info("Usuario: " + username + " baneado correctamente.");
			inicializar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Notification.warning(e.getMessage());
		}
	}
	
	public void actionListenerDesbanearUsuario(String username) {
		try {
			m.desBanearUsuario(username);
			Notification.info("Usuario: " + username + " desbaneado correctamente.");
			inicializar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Notification.warning(e.getMessage());
		}
	}

	public void modificarUsuario() {
		try {
			m.modificarUsuario(personaModificada.getUsername(), personaModificada.getEstado().getIdEstado(),
					personaModificada.getCorreoElectronico(), personaModificada.getContrasenia(),
					personaModificada.getNombre(), personaModificada.getApellido());
			Notification.info("Usuario: " + personaModificada.getUsername() + " modificado/a coorectamente.");
			inicializar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Notification.warning(e.getMessage());
		}
	}
	
	public void actionListenerEliminarUsuario(String username) {
		try {
			m.eliminarUsuario(username);
			Notification.info("Usuario: " + username + " eliminado correctamente.");
			inicializar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Notification.warning(e.getMessage());
		}
	}

	public Persona getPersonaModificada() {
		return personaModificada;
	}

	public void setPersonaModificada(Persona personaModificada) {
		this.personaModificada = personaModificada;
	}

	public List<Persona> getListapersona() {
		return listapersona;
	}

	public void setListapersona(List<Persona> listapersona) {
		this.listapersona = listapersona;
	}

}
