package foodstarWeb.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import foodstarEJB.model.managers.ManagerRestaurante;

@Named
@SessionScoped
public class BeanIndexUsuario implements Serializable {
	@EJB
	private ManagerRestaurante mRestaurante;
	public BeanIndexUsuario() {
		// TODO Auto-generated constructor stub
	}

	public String actionCerrarSesion() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/index?faces-redirect=true";
	}
	
	public String actionRegistrarRestaurantes() {
		return "registrarRestaurantes?faces-redirect=true";
	}
}
