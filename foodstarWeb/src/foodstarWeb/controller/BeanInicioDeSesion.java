package foodstarWeb.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;


import foodstarEJB.model.managers.ManagerPersona;
import foodstarWeb.view.util.Notification;

@Named
@SessionScoped
public class BeanInicioDeSesion implements Serializable {
	@EJB
	private ManagerPersona m;
	
	private String nombreDeUsuario;
	private String contrasenia;
	
	public BeanInicioDeSesion() {
		// TODO Auto-generated constructor stub
	}

	public String actionRegresar() {
		return "index?faces-redirect=true";
	}
	
	public String actionIniciarSesion() {
		try {
			if (m.iniciarSesion(nombreDeUsuario, contrasenia)) {
				System.out.println(nombreDeUsuario);
				Notification.info("Bienvenido administrador");
				return "administrador/indexAdministrador?faces-redirect=true";
			} else {
				Notification.info("Bienvenido usuario");
				System.out.println(nombreDeUsuario);
				return "usuario/indexUsuario?faces-redirect=true";
			}
			
		} catch (Exception e) {
			// TODO Auto-generated 	catch block
			e.printStackTrace();
			Notification.warning(e.getMessage());
			return "";
		}
	}

	public String getNombreDeUsuario() {
		return nombreDeUsuario;
	}

	public void setNombreDeUsuario(String nombreDeUsuario) {
		this.nombreDeUsuario = nombreDeUsuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	
	
}
