package foodstarWeb.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import foodstarEJB.model.managers.ManagerPersona;

@Named
@SessionScoped
public class BeanIndexAdministrador implements Serializable {
	@EJB
	private ManagerPersona m;

	public BeanIndexAdministrador() {
		// TODO Auto-generated constructor stub
	}

	public String actionCerrarSesion() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/index?faces-redirect=true";
	}

	public String actionAdministrarUsuarios() {
		return "administrarUsuarios?faces-redirect=true";
	}

	public String actionAdministrarRestaurantes() {
		return "administrarRestaurantes?faces-redirect=true";
	}
}
